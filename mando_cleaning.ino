

#define NUM_BUTTONS 8
int input_pins[NUM_BUTTONS] = { 3,4,5,6,7,8,9,10 };

boolean last_button_states[NUM_BUTTONS];
boolean button_states[NUM_BUTTONS];
char keys[NUM_BUTTONS] = {'a', 'b', 's', 'l', 0xDA,  0xD9, 0xD8, 0xD7};

void setup()
{
  for(int i = 0; i < NUM_BUTTONS; i++)
  {
    pinMode(input_pins[i], INPUT);
  }

}

void loop()
{
   for(int i = 0; i < NUM_BUTTONS; i++)
  {
     button_states[i] = digitalRead(input_pins[i]);
    
  }
   for(int i = 0; i < NUM_BUTTONS; i++)
  {
    if (button_states[i] == HIGH and last_button_states[i] == LOW) {
      // If the current state is HIGH then the button
      // Send to serial that the button is pressed:
     Keyboard.press(keys[i]);  
    } else if (button_states[i] == LOW and last_button_states[i] == HIGH) {
     Keyboard.release(keys[i]);
  }
  }
   for(int i = 0; i < NUM_BUTTONS; i++)
     {
       last_button_states[i] = button_states[i];
     }
}
